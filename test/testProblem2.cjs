const inventory = require('../cars');
const findLastCar = require('../problem2.cjs');
const lastCar = findLastCar(inventory);
console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);