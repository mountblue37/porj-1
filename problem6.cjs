function getBMWandAudi(arr=[], m1, m2) {
    let filteredCars = [];
    if(!Array.isArray(arr) || m1 == undefined || m2 == undefined) return filteredCars;
    for(i = 0; i < arr.length; i++)
        if(arr[i].car_make === m1 || arr[i].car_make === m2) filteredCars.push(arr[i]);
    return filteredCars;
}

module.exports = getBMWandAudi;