function carsOlderThan(arr=[], year) {
    let olderThan = [];
    if(!Array.isArray(arr) || year == undefined) return olderThan;
    for(i = 0; i < arr.length; i++)
        if(arr[i] < year) olderThan.push(arr[i]);
    return olderThan;
}

module.exports = carsOlderThan;