function findCarById(arr=[], id) {
    let result = [];
    if(!Array.isArray(arr) || id == undefined) return result;
    for(i = 0; i < arr.length; i++) {
        if(arr[i].id === id) {
            result = arr[i];
            break;
        }
    }
    return result;
}

module.exports = findCarById;