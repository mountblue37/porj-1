function findLastCar(arr =[]) {
    if(!Array.isArray(arr)) return [];
    return arr[arr.length - 1];
}

module.exports = findLastCar;