function getAllYears(arr=[]) {
    let allYears = [];
    if(!Array.isArray(arr)) return allYears;
    for(i = 0; i < arr.length; i++)
        allYears.push(arr[i].car_year);
    return allYears;
}

module.exports = getAllYears;