function sortByModel(arr=[]) {
    if(!Array.isArray(arr)) return [];
    let sortedArr = [...arr];
    sortedArr.sort((a,b) =>  {
        if(a.car_model.toLowerCase() > b.car_model.toLowerCase()) return 1;
        if(a.car_model.toLowerCase() < b.car_model.toLowerCase()) return -1;
        if(a.car_model.toLowerCase() == b.car_model.toLowerCase()) return 0;
    });

    return sortedArr;
}

module.exports = sortByModel;